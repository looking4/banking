# Реализация REST API по работе со счетами

**Описание счета**

Счет реализован в виде POJO класс *Account* c двумя полями: *accountNumber* типа *String*
и *balance* типа *Long*. Уникальным идентификатором счета является поле *accountNumber*. 

**Возможные запросы:**
1) *POST /bankaccount/{id}* - Завести новый счет. На вход команда принимает
параметр номер счета - 5-ти значное число (соответствует параметру id в запросе). 

2) *PUT /bankaccount/{id}/deposit* - Внести сумму на счёт. На вход команда принимает 2
параметра - номер счета (параметр id) и сумму к зачислению (указывается в теле запроса).
 
3) *PUT /bankaccount/{id}/withdraw* - Снять сумму со счёта. На вход команда принимает
2 параметра - номер счета (параметр id) и сумму снятия (указывается в теле запроса).

4) *GET /bankaccount/{id}/balance* - Узнать баланс. На вход команда принимает
параметр номер счета - 5-ти значное число (соответствует параметру id в запросе). Результат - число типа Long.

**Особенности сервиса**:

1) Нельзя завести 2 раза аккаунт с одним номером

2) Перед тем, как проводить операции со счетом, его надо зарегистрировать

3) Нельзя класть на счёт и снимать отрицательные суммы

4) Значение счета не может опускаться ниже 0

В случае нарушения одного из вышеприведенных пунктов будет отправлен JSON объект
с описание ошибки.

Сервис запускается на порту **8282** и использует СУБД H2 для хранения данных.

**Настройка среды**

1. Наличие Java версии не ниже 8 версии
2. Корректно прописанные пути в переменной PATH к JDK (введите команду *java -version* для проверки)
3. Наличие Maven не ниже 3 версии
4. Корректно прописанные пути в переменной PATH к Maven (введите команду *mvn -v* для проверки)

**Запуск тестов**

Открыть консоль в директории проекта и ввести команду *mvn clean test*.
В случае успешного прохождения тестов будет выведено сообщение *BUILD SUCCESS*.

**Запуск сервиса**

Открыть консоль в директории проекта и ввести команду
*mvn spring-boot:run*. В случае успешного запуска будет 
выведено сообщение *Started BankingApplication in x.xx seconds*. 

