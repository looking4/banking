package ru.looking4.banking.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.looking4.banking.model.Account;

/**
 * @author reyzor
 * @version 1.0
 * @since 12.10.2018
 */
public interface AccountRepository extends JpaRepository<Account, String> {
}
