package ru.looking4.banking.error;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author reyzor
 * @version 1.0
 * @since 12.10.2018
 */

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class AccountInvalidNumber extends RuntimeException {

    public AccountInvalidNumber() {

        super("Неверный номер аккаунта! Аккаунт должен содержать 5 цифр!");
    }
}
