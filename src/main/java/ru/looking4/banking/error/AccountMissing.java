package ru.looking4.banking.error;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author reyzor
 * @version 1.0
 * @since 12.10.2018
 */

@ResponseStatus(HttpStatus.NOT_FOUND)
public class AccountMissing extends RuntimeException {

    public AccountMissing() {

        super("Аккаунт отсутствует в базе данных!");
    }
}
