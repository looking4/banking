package ru.looking4.banking.error;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author reyzor
 * @version 1.0
 * @since 12.10.2018
 */

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class IncorrectDepositWithdrawValue extends RuntimeException {

    public IncorrectDepositWithdrawValue() {

        super("Некорректно введена сумма для начисления/снятия!");
    }
}
