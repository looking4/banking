package ru.looking4.banking.error;

import java.time.LocalDateTime;

/**
 * @author reyzor
 * @version 1.0
 * @since 12.10.2018
 */

public class ExceptionInfo {

    private LocalDateTime timeOfException;
    private String messageOfException;

    public ExceptionInfo(LocalDateTime timeOfException, String messageOfException) {

        this.timeOfException = timeOfException;
        this.messageOfException = messageOfException;
    }

    public LocalDateTime getTimeOfException() {
        return timeOfException;
    }

    public String getMessageOfException() {
        return messageOfException;
    }
}
