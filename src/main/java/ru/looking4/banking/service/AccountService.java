package ru.looking4.banking.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import ru.looking4.banking.error.AccountInvalidNumber;
import ru.looking4.banking.error.AccountMissing;
import ru.looking4.banking.error.AccountOverlap;
import ru.looking4.banking.error.IncorrectDepositWithdrawValue;
import ru.looking4.banking.model.Account;
import ru.looking4.banking.repository.AccountRepository;

import java.util.Optional;

/**
 * @author reyzor
 * @version 1.0
 * @since 12.10.2018
 */
@RestController
@RequestMapping(value = "/bankaccount")
public class AccountService {

    private AccountRepository accountRepository;

    @Autowired
    public AccountService(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Transactional
    @PostMapping("/{id}")
    public void createAccount(@PathVariable("id") String numberOfAccount) {

        if (!checkAccountNumber(numberOfAccount)) throw new AccountInvalidNumber();

        final Optional<Account> accountOptional = accountRepository.findById(numberOfAccount);
        if (accountOptional.isPresent()) throw new AccountOverlap();

        accountRepository.save(new Account(numberOfAccount));
    }

    @Transactional
    @PutMapping("/{id}/deposit")
    public void depositOnAccount(@PathVariable("id") String numberOfAccount, @RequestBody String valueOfDeposit) {

        if (!checkValueDepositWithdraw(valueOfDeposit)) throw new IncorrectDepositWithdrawValue();

        if (!checkAccountNumber(numberOfAccount)) throw new AccountInvalidNumber();

        final Optional<Account> accountOptional = accountRepository.findById(numberOfAccount);
        if (!accountOptional.isPresent()) throw new AccountMissing();

        final Account account = accountOptional.get();
        account.deposit(Long.parseLong(valueOfDeposit));
        accountRepository.save(account);
    }

    @Transactional
    @PutMapping("/{id}/withdraw")
    public void withdrawFromAccount(@PathVariable("id") String numberOfAccount, @RequestBody String valueOfWithdraw) {

        if (!checkValueDepositWithdraw(valueOfWithdraw)) throw new IncorrectDepositWithdrawValue();

        if (!checkAccountNumber(numberOfAccount)) throw new AccountInvalidNumber();

        final Optional<Account> accountOptional = accountRepository.findById(numberOfAccount);
        if (!accountOptional.isPresent()) throw new AccountMissing();

        final Account account = accountOptional.get();
        if (account.getBalance() < Long.parseLong(valueOfWithdraw)) throw new IncorrectDepositWithdrawValue();

        account.withdraw(Long.parseLong(valueOfWithdraw));
        accountRepository.save(account);
    }

    @Transactional
    @GetMapping("/{id}/balance")
    public Long getBalance(@PathVariable("id") String numberOfAccount) {

        if (!checkAccountNumber(numberOfAccount)) throw new AccountInvalidNumber();

        final Optional<Account> accountOptional = accountRepository.findById(numberOfAccount);
        if (!accountOptional.isPresent()) throw new AccountMissing();

        return accountOptional.get().getBalance();
    }

    public boolean checkAccountNumber(String numberOfAccount) {

        if (numberOfAccount.length() != 5) return false;
        try {
            Integer.parseInt(numberOfAccount);
        } catch (NumberFormatException e) {
            throw new AccountInvalidNumber();
        }
        return true;
    }

    public boolean checkValueDepositWithdraw(String value) {

        Long valueToLong = null;
        try {
            valueToLong = Long.parseLong(value);
        } catch (NumberFormatException e) {
            throw new IncorrectDepositWithdrawValue();
        }
        if (valueToLong < 1) return false;
        return true;
    }
}
