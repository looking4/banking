package ru.looking4.banking.service;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import ru.looking4.banking.error.*;

import java.time.LocalDateTime;

/**
 * @author reyzor
 * @version 1.0
 * @since 12.10.2018
 */

@ControllerAdvice
@RestController
public class ResponseEntityExceptionHandlerForAccount extends ResponseEntityExceptionHandler {

    @ExceptionHandler(Exception.class)
    public final ResponseEntity<Object> handleUnpredictableException(Exception exception, WebRequest request) {

        return new ResponseEntity<>(createExceptionInfo(exception), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(AccountInvalidNumber.class)
    public final ResponseEntity<Object> handleAccountInvalidNumber(AccountInvalidNumber exception, WebRequest request) {

        return new ResponseEntity<>(createExceptionInfo(exception), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(AccountMissing.class)
    public final ResponseEntity<Object> handleAccountMissing(AccountMissing exception, WebRequest request) {

        return new ResponseEntity<>(createExceptionInfo(exception), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(AccountOverlap.class)
    public final ResponseEntity<Object> handleAccountOverlap(AccountOverlap exception, WebRequest request) {

        return new ResponseEntity<>(createExceptionInfo(exception), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(IncorrectDepositWithdrawValue.class)
    public final ResponseEntity<Object> handleIncorrectDepositWithdrawValue(IncorrectDepositWithdrawValue exception, WebRequest request) {

        return new ResponseEntity<>(createExceptionInfo(exception), HttpStatus.BAD_REQUEST);
    }

    private ExceptionInfo createExceptionInfo(Exception exception) {
        return new ExceptionInfo(LocalDateTime.now(), exception.getMessage());
    }
}
