package ru.looking4.banking.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author reyzor
 * @version 1.0
 * @since 12.10.2018
 */
@Entity
@Table(name = "accounts")
public class Account {

    @Id
    @Column(name = "account_number")
    private String accountNumber;
    @Column(name = "balance")
    private Long balance;

    public Account() {
    }

    public Account(String accountNumber, Long balance) {

        this.accountNumber = accountNumber;
        this.balance = balance;
    }

    public Account(String accountNumber) {

        this(accountNumber, 0l);
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public Long getBalance() {
        return balance;
    }

    public void setBalance(Long balance) {
        this.balance = balance;
    }

    public void deposit(Long valueOfDeposit) {

        this.balance += valueOfDeposit;
    }

    public void withdraw(Long valueOfWithdraw) {

        this.balance -= valueOfWithdraw;
    }

    @Override
    public String toString() {
        return "Account{" +
                "accountNumber=" + accountNumber +
                ", balance=" + balance +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Account account = (Account) o;

        return accountNumber != null ? accountNumber.equals(account.accountNumber) : account.accountNumber == null;
    }

    @Override
    public int hashCode() {
        return accountNumber != null ? accountNumber.hashCode() : 0;
    }
}
