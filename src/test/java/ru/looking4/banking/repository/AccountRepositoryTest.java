package ru.looking4.banking.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import ru.looking4.banking.model.Account;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AccountRepositoryTest {

    @Autowired
    private AccountRepository accountRepository;

    @Test
    public void saveAccountTest() {

        accountRepository.save(new Account("12345"));
        assertFalse(accountRepository.findAll().isEmpty());
    }

    @Test
    public void deleteAccountTest() {

        final Account account = new Account("12345");
        accountRepository.save(account);
        assertFalse(accountRepository.findAll().isEmpty());
        accountRepository.delete(account);
        assertTrue(accountRepository.findAll().isEmpty());
    }
}