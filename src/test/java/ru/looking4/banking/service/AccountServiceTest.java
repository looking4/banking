package ru.looking4.banking.service;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import ru.looking4.banking.error.AccountInvalidNumber;
import ru.looking4.banking.error.AccountOverlap;
import ru.looking4.banking.error.IncorrectDepositWithdrawValue;
import ru.looking4.banking.repository.AccountRepository;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AccountServiceTest {

    @Autowired
    private AccountService accountService;

    @Autowired
    private AccountRepository accountRepository;

    @After
    public void deleteAccount() {

        if (accountRepository.existsById("12345")) accountRepository.deleteById("12345");
    }

    @Test
    public void createAccount() {

        accountService.createAccount("12345");
    }

    @Test(expected = AccountInvalidNumber.class)
    public void createAccountWithInvalidNumberWithLetter() {

        accountService.createAccount("f12345");
    }

    @Test(expected = AccountInvalidNumber.class)
    public void createAccountWithInvalidNumberLessThen5Numbers() {

        accountService.createAccount("123");
    }

    @Test(expected = AccountInvalidNumber.class)
    public void createAccountWithInvalidNumberMoreThen5Numbers() {

        accountService.createAccount("123123");
    }

    @Test
    public void depositOnAccountWithValidData() {

        accountService.createAccount("12345");
        assertTrue(accountService.getBalance("12345").equals(0l));
        accountService.depositOnAccount("12345", "100");
        assertTrue(accountService.getBalance("12345").equals(100l));
    }

    @Test(expected = IncorrectDepositWithdrawValue.class)
    public void depositOnAccountNegativeArg() {

        accountService.createAccount("12345");
        accountService.depositOnAccount("12345", "-12");
    }

    @Test(expected = IncorrectDepositWithdrawValue.class)
    public void depositOnAccountWithLetters() {

        accountService.createAccount("12345");
        accountService.depositOnAccount("12345", "fdsf");
    }

    @Test
    public void withdrawFromAccountWithValidData() {

        accountService.createAccount("12345");
        assertTrue(accountService.getBalance("12345").equals(0l));
        accountService.depositOnAccount("12345", "201");
        accountService.withdrawFromAccount("12345", "100");
        assertTrue(accountService.getBalance("12345").equals(101l));
    }

    @Test(expected = IncorrectDepositWithdrawValue.class)
    public void withdrawOnAccountNegativeArg() {

        accountService.createAccount("12345");
        accountService.withdrawFromAccount("12345", "-12");
    }

    @Test(expected = IncorrectDepositWithdrawValue.class)
    public void withdrawOnAccountWithLetters() {

        accountService.createAccount("12345");
        accountService.withdrawFromAccount("12345", "fdsf");
    }

    @Test
    public void getBalance() {

        accountService.createAccount("12345");
        assertTrue(accountService.getBalance("12345").equals(0l));
        accountService.depositOnAccount("12345", "100");
        assertTrue(accountService.getBalance("12345").equals(100l));
    }

    @Test(expected = AccountOverlap.class)
    public void createAccountAccountOverlap() {

        accountService.createAccount("12345");
        accountService.createAccount("12345");
    }

    @Test
    public void checkAccountNumberUseNot5Sign() {

        assertFalse(accountService.checkAccountNumber("123"));
        assertFalse(accountService.checkAccountNumber("123123"));
    }

    @Test(expected = AccountInvalidNumber.class)
    public void checkAccountNumberAccountInvalidNumber() {

        accountService.createAccount("fdsfds");
    }

    @Test
    public void checkValueDepositWithdraw() {

        assertTrue(accountService.checkValueDepositWithdraw("123"));
        assertFalse(accountService.checkValueDepositWithdraw("-1212"));
    }

    @Test(expected = IncorrectDepositWithdrawValue.class)
    public void checkValueDepositWithdrawResultWithException() {

        accountService.checkValueDepositWithdraw("fdsfds");
    }
}